import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *

def lineSegment():
    glClear(GL_COLOR_BUFFER_BIT)
    glBegin(GL_LINES)
    glColor(1.0, 0.0, 0.0)
    glVertex(30, 50)
    glVertex(100, 150)
    glEnd()

def main():
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
    pygame.display.set_caption("A simple line")

    glClearColor(1.0, 1.0, 1.0, 0.0)
    #set the background colour to be white
    
    gluOrtho2D(0.0, 300.0, 0.0, 200.0)
    lineSegment()
    #calling the function that will actualy
    pygame.display.flip()
    pygame.time.wait(10)

main()

